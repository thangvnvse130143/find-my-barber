package com.thang.findmybarber

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import com.thang.findmybarber.adapter.ServiceAdapter
import com.thang.findmybarber.model.Store
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.activity_salon.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SalonActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_salon)

        val salon = intent.extras?.get("salon") as Store
        service_name.text = salon.name
        salon_address.text = salon.address
        salon_rating_bar.rating = salon.rating.toFloat()
        salon_rating.text = salon.rating.toString()
//        salon_rating_number.text = "Đã có ${salon.ratingNumber} lượt đánh giá"
        if (salon.banner.isNotEmpty())
            Picasso.get().load(salon.banner).into(salon_image)
        else {
            salon_image.setImageResource(salon.shopImg)
        }

        MainScope().launch {
            loading.show()
            val services = AppRepository().getServices()
            loading.hide()
            salon_rvc.adapter = ServiceAdapter(services.toMutableList())
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
