package com.thang.findmybarber.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smarteist.autoimageslider.SliderViewAdapter
import com.thang.findmybarber.R
import com.thang.findmybarber.ServiceDetailActivity
import com.thang.findmybarber.model.Service
import kotlinx.android.synthetic.main.image_slider_item.view.*

class ImageSliderAdapter(val images: List<Int>, val services: List<Service>): SliderViewAdapter<ImageSliderAdapter.ImageSliderViewHolder>() {
    class ImageSliderViewHolder(val itemView: View) : SliderViewAdapter.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup?): ImageSliderViewHolder {
        val view = LayoutInflater.from(parent?.context)
            .inflate(R.layout.image_slider_item, parent, false)
        return ImageSliderViewHolder(view)
    }

    override fun getCount(): Int = images.size

    override fun onBindViewHolder(viewHolder: ImageSliderViewHolder?, position: Int) {
        viewHolder?.let {
            it.itemView.imageView_slide.setImageResource(images[position])
            it.itemView.setOnClickListener {
                val intent = Intent(it.context, ServiceDetailActivity::class.java)
                try {
                    intent.putExtra("service", services[position])
                } catch (e: Exception) {
                    intent.putExtra("service", services[0])
                }
                it.context.startActivity(intent)
            }
        }
    }
}