package com.thang.findmybarber.adapter

import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thang.findmybarber.R
import com.thang.findmybarber.model.Review
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.rating_item.view.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class RatingAdapter(private val ratings: MutableList<Review>, private val userId: Int) :
    RecyclerView.Adapter<RatingAdapter.RatingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatingViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.rating_item, parent, false)
        return RatingViewHolder(view)
    }

    override fun getItemCount(): Int = ratings.size

    override fun onBindViewHolder(holder: RatingViewHolder, position: Int) {
        val rating = ratings[position]
        holder.itemView.run {
            rating_username.text = rating.user?.name
            rating_rating_bar.rating = rating.mark.toFloat()
            content.inputType = InputType.TYPE_NULL
            content.setText(rating.review)
            if (rating.user_id == userId) {
                btn_edit.visibility = View.VISIBLE
                btn_edit.setOnClickListener {
                    enableRating(this)
                    rating_btn.setOnClickListener {
                        disableRating(this)
                        MainScope().launch {
                            AppRepository().updateReview(
                                rating.copy(
                                    review = content.text.toString(),
                                    mark = rating_rating_bar.rating.toDouble()
                                )
                            )
                            val review = AppRepository().getServiceReviews(rating.service_id).first { it.id == rating.id }
                            ratings[position] = review
                            notifyItemChanged(position)
                        }
                    }
                    cancel_btn.setOnClickListener {
                        disableRating(this)
                    }
                }
            }
        }
    }

    private fun enableRating(view: View) {
        view.apply {
            rating_btn.visibility = View.VISIBLE
            cancel_btn.visibility = View.VISIBLE
            content.inputType = InputType.TYPE_CLASS_TEXT
            content.background = context.getDrawable(R.drawable.border_radius_8)
            content.requestFocus()
            rating_rating_bar.setIsIndicator(false)
        }
    }

    private fun disableRating(view: View) {
        view.apply {
            rating_btn.visibility = View.INVISIBLE
            cancel_btn.visibility = View.INVISIBLE
            content.inputType = InputType.TYPE_NULL
            content.background = null
        }
    }

    class RatingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}