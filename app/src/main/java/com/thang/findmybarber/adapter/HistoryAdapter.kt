package com.thang.findmybarber.adapter

import android.accessibilityservice.AccessibilityButtonController
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.thang.findmybarber.R
import com.thang.findmybarber.ServiceDetailActivity
import com.thang.findmybarber.SuccessActivity
import com.thang.findmybarber.formatPrice
import com.thang.findmybarber.model.Booking
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.history_item_2.view.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.Instant

class BookingAdapter(private val histories: MutableList<Booking>) :
    RecyclerView.Adapter<BookingAdapter.BookingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookingViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.history_item_2, parent, false)
        return BookingViewHolder(view)
    }

    override fun getItemCount(): Int = histories.size

    override fun onBindViewHolder(holder: BookingViewHolder, position: Int) {
        val view = holder.itemView
        val booking = histories[position]

        view.run {
            service_name.text = booking.service?.name
            service_price.text = "${formatPrice(booking.price)}đ"
            order_time.text = "${SimpleDateFormat("dd-MM-yyyy hh:mm").format(
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(booking.service_time)
            )}"
//            order_time.text = "2020-04-08 08:30"
            booking_status.text = when (booking.status) {
                Booking.PENDING -> "Đã đặt"
                Booking.CANCEL -> {
                    cancel_btn.visibility = View.INVISIBLE
                    "Đã huỷ"
                }
                Booking.DONE -> {
                    cancel_btn.visibility = View.INVISIBLE
                    "Đã hoàn thành"
                }
                Booking.APPROVED -> {
                    "Đã Xác nhận"
                }
                else -> ""
            }
            setOnClickListener {
                val intent = Intent(it.context, SuccessActivity::class.java)
                intent.putExtra("booking", booking)
                it.context.startActivity(intent)
            }

            cancel_btn.setOnClickListener {
                showConfirmDialog(context, booking, onPositiveButtonCallback = {
                    histories.set(position, it)
                    notifyItemChanged(position)
                })
            }
        }
    }

    private fun showConfirmDialog(context: Context, booking: Booking, onPositiveButtonCallback: (Booking) -> Unit) {
        AlertDialog.Builder(context)
            .setTitle("Huỷ lịch")
            .setMessage("Bạn có muốn huỷ lịch ${booking.service?.name}?")
            .setPositiveButton("Có") { dialog, which ->
                MainScope().launch {
                    val newBooking = booking.copy(status = Booking.CANCEL)
                    AppRepository().updateBook( newBooking)
                    onPositiveButtonCallback(newBooking)
                }

            }.setNegativeButton("Không") { _, _ -> }.show()
    }

    class BookingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}