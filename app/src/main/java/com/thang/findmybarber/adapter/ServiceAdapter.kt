package com.thang.findmybarber.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.thang.findmybarber.R
import com.thang.findmybarber.ServiceDetailActivity
import com.thang.findmybarber.formatPrice
import com.thang.findmybarber.model.Service
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.service_item_store.view.*

class ServiceAdapter(
    val services: MutableList<Service>,
    private val layout: Int = R.layout.service_item
) :
    RecyclerView.Adapter<ServiceAdapter.ServiceViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(layout, parent, false)
        return ServiceViewHolder(view)
    }

    override fun getItemCount(): Int = services.size

    override fun onBindViewHolder(holder: ServiceViewHolder, position: Int) {
        val view = holder.itemView
        val service = services[position]

        view.run {
            service_name.text = service.name
            service_duration.text = "${service.duration} phút"
            shop_name.text = service.shop?.name
            shop_address?.text = service.shop?.address
            shop_distance.text = "${AppRepository.formatDistance(service.shop?.distance!!)}km"
            if (service.promotion_price != 0) {
                service_price_crossed.text = "${formatPrice(service.price)}đ"
                service_price.text = "${formatPrice(service.promotion_price)}đ"
            } else {
                service_price_crossed.visibility = View.GONE
                service_price.text = "${formatPrice(service.price)}đ"
            }
            if (!service.image.isNullOrEmpty()){
                val image = Picasso.get().load(service.image)
                image?.into(service_image)
            } else
                service.dummy_image?.let { service_image.setImageResource(it) }
            if (service.discount == null || service.discount == 0)
                service_discount?.visibility = View.GONE
            else service_discount?.text = "${service.discount}%"
            rating_number?.text = "${service.ratingNumber}"
            service.rating?.let { rating_bar?.rating = it.toFloat() }
            setOnClickListener {
                it.context.apply {
                    val sharedPreferences = getSharedPreferences(packageName, Context.MODE_PRIVATE)
                    sharedPreferences.edit().putStringSet("viewedIds",
                        sharedPreferences.getStringSet("viewedIds", HashSet<String>())!!.apply {
                            add(service.id.toString())
                        }).apply()
                    val intent = Intent(this, ServiceDetailActivity::class.java)
                    intent.putExtra("service", service)
                    startActivity(intent)
                }
            }
        }
    }

    class ServiceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}