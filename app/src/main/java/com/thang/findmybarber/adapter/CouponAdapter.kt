package com.thang.findmybarber.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thang.findmybarber.R
import com.thang.findmybarber.SalonActivity
import com.thang.findmybarber.ServiceDetailActivity
import com.thang.findmybarber.model.Coupon
import kotlinx.android.synthetic.main.coupon_item.view.*
import kotlinx.android.synthetic.main.coupon_item2.view.*
import java.text.SimpleDateFormat

class CouponAdapter(private val coupons: MutableList<Coupon>) :
    RecyclerView.Adapter<CouponAdapter.CouponViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CouponViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.coupon_item2, parent, false)
        return CouponViewHolder(view)
    }

    override fun getItemCount(): Int = coupons.size

    override fun onBindViewHolder(holder: CouponViewHolder, position: Int) {
        val view = holder.itemView
        val coupon = coupons[position]

//        view.run {
//            coupon_name.text = coupon.name
//            coupon_shop_service_name.text = "${coupon.serviceName}"
//            coupon_shop_name_text.text = "${coupon.shopName}"
//            coupon_discount_price.text = "${coupon.discountPrice?.toInt()}đ"
//            val sdf = SimpleDateFormat("dd/MM/yy")
//            coupon_deadline.text = "Thời hạn: ${sdf.format(coupon.deadline?.time)}"
//
//            setOnClickListener {
//                val intent = Intent(it.context, ServiceDetailActivity::class.java)
//                it.context.startActivity(intent)
//            }
//        }
        coupon.img?.let { view.coupon_image.setImageResource(it) }
        view.setOnClickListener {
            val context = view.context
            context.startActivity(Intent(context, SalonActivity::class.java))
        }
    }

    class CouponViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}