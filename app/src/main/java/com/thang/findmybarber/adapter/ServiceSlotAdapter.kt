package com.thang.findmybarber.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.thang.findmybarber.R
import com.thang.findmybarber.model.Slot
import com.thang.findmybarber.model.SlotStatus
import kotlinx.android.synthetic.main.service_slot_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class ServiceSlotAdapter(private val slots: MutableList<Slot>, val onClick: (Slot) -> Unit) :
    RecyclerView.Adapter<ServiceSlotAdapter.ServiceSlotViewHolder>() {
    var selectedPos = -1

    private val dateFormat by lazy { SimpleDateFormat("HH:mm") }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceSlotViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.service_slot_item, parent, false)
        view.run {
            when (viewType) {
                0 -> slot_time.setTextColor(resources.getColor(android.R.color.darker_gray))
                1 -> {
                    slot_time.backgroundTintList =
                        ContextCompat.getColorStateList(context, R.color.colorPrimary)
                    slot_time.setTextColor(resources.getColor(R.color.colorOnPrimary))
                }
            }
        }
        return ServiceSlotViewHolder(view)
    }

    override fun getItemCount(): Int = slots.size

    override fun getItemViewType(position: Int): Int {
        if (slots[position].status == SlotStatus.UNSELECTABLE) return 0
        if (position == selectedPos) return 1
        return 2
    }

    override fun onBindViewHolder(holder: ServiceSlotViewHolder, position: Int) {
        val slot = slots[position]

        holder.itemView.run {
            slot_time.text = dateFormat.format(slot.time.time)
            Log.i("slotItem", "${slot.time.get(Calendar.HOUR)}:${slot.time.get(Calendar.MINUTE)}")
            setOnClickListener {
                selectedPos = if (selectedPos == position) -1 else position
                notifyDataSetChanged()
                onClick(slot)
            }

            if (slot.discount != null) {
                slot_discount.visibility = View.VISIBLE
                slot_discount.text = "${slot.discount}%"
            } else {
                slot_discount.visibility = View.INVISIBLE
            }
        }
    }

    class ServiceSlotViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}