package com.thang.findmybarber

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.thang.findmybarber.model.User
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch


class LoginActivity : AppCompatActivity(), ActivityCompat.OnRequestPermissionsResultCallback {

    companion object {
        /**
         * Request code for location permission request.
         *
         * @see .onRequestPermissionsResult
         */
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    private val REQUEST_SIGNUP = 0
    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                // Permission is granted. Continue the action or workflow in your
                // app.
            } else {
                // Explain to the user that the feature is unavailable because the
                // features requires a permission that the user has denied. At the
                // same time, respect the user's decision. Don't link to system
                // settings in an effort to convince the user to change their
                // decision.
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        ApplicationContext.instance?.init(applicationContext)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissionLauncher.launch(
                Manifest.permission.ACCESS_FINE_LOCATION)
            requestPermissionLauncher.launch(
                Manifest.permission.ACCESS_COARSE_LOCATION)
        }

        btn_login.setOnClickListener {
            validate()
        }

        link_signup.setOnClickListener {
            val intent = Intent(applicationContext, SignUpActivity::class.java)
            startActivityForResult(intent, REQUEST_SIGNUP)
        }
        link_forgot_password.setOnClickListener {
            startActivity(Intent(this, ForgotPasswordActivity::class.java))
        }
    }

    private fun onLoginSuccess(user: User) {
        getSharedPreferences(packageName, Context.MODE_PRIVATE).edit().apply {
            putInt("userId", user.id!!)
            putString("username", user.username)
            apply()
        }
        startActivity(Intent(this, MainActivity::class.java))
    }

    private fun validate() {
        var valid = true
        val username: String = input_username.text.toString()
        val password: String = input_password.text.toString()
        if (username.isEmpty()) {
            input_username.error = "Tên đăng nhập không thể trống"
            valid = false
        } else {
            input_username.error = null
        }
        if (password.isEmpty()) {
            input_password.error = "Mật khẩu không thể trống"
            valid = false
        } else {
            input_password.error = null
        }
        if (valid)
            MainScope().launch {
                loading?.show()
                val user = AppRepository().login(User(username = username, password = password))
                if (user != null && user.username?.isNotBlank()!!) onLoginSuccess(user)
                else input_username.error = "Sai tên đăng nhập hoặc mật khẩu"
                loading?.hide()
            }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) { // TODO: Implement successful signup logic here
//                onLoginSuccess()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
