package com.thang.findmybarber

import android.app.SearchManager
import android.content.ComponentName
import android.content.Context
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import com.thang.findmybarber.fragments.CouponFragment
import com.thang.findmybarber.fragments.BookingFragment
import com.thang.findmybarber.fragments.HomeFragment
import com.thang.findmybarber.fragments.PersonFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    var optionMenu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottom_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_home -> showSearchMenu()
                else -> hideSearchMenu()
            }
            when (it.itemId) {
                R.id.navigation_home -> {
                    supportActionBar?.title = "Trang chủ"
                    loadFragment(HomeFragment())
                    true
                }
//                R.id.navigation_coupon -> {
//                    supportActionBar?.title = "Ưu đãi"
//                    loadFragment(CouponFragment())
//                    true
//                }
                R.id.navigation_booking -> {

                    supportActionBar?.title = "Lịch sử"
                    loadFragment(BookingFragment())
                    true
                }
                R.id.navigation_personal -> {
                    supportActionBar?.title = "Cá nhân"
                    loadFragment(PersonFragment())
                    true
                }
                else -> false
            }
        }
        bottom_navigation.selectedItemId = R.id.navigation_home
    }

    private fun showSearchMenu() {
        optionMenu?.findItem(R.id.app_bar_search)?.isVisible = true
    }
    private fun hideSearchMenu() {
        optionMenu?.findItem(R.id.app_bar_search)?.isVisible = false
    }

    override fun onBackPressed() {
        finish()
        super.onBackPressed()
    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        optionMenu = menu
        // Get the SearchView and set the searchable configuration
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu?.findItem(R.id.app_bar_search)?.actionView as SearchView).apply {
            // Assumes current activity is the searchable activity
            isSubmitButtonEnabled = true
            val componentName = ComponentName(context, SearchActivity::class.java)
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
//            setIconifiedByDefault(false) // Do not iconify the widget; expand it by default
        }
        return super.onCreateOptionsMenu(menu)
    }
}
