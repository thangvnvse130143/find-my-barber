package com.thang.findmybarber

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.thang.findmybarber.adapter.ServiceAdapter
import com.thang.findmybarber.adapter.ServiceTypeAdapter
import com.thang.findmybarber.dialog.SortDialog
import com.thang.findmybarber.dialog.SortDialog.Sort.*
import com.thang.findmybarber.model.ServiceType
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.activity_service.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch


class ServiceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service)

        val serviceTypes = AppRepository().getServiceType()
        val selectedServiceType = intent.getSerializableExtra("serviceType") as ServiceType

        MainScope().launch {
            loading?.show()
            val services = AppRepository().getServicesByType(selectedServiceType.id!!).toMutableList()
            loading?.hide()
            val serviceAdapter = ServiceAdapter(services, R.layout.service_item_store)
            val serviceTypeAdapter =
                ServiceTypeAdapter(serviceTypes, R.layout.service_type_horizontal) {
                    // on item click
                    MainScope().launch {
                        loading?.show()
                        services.clear()
                        services.addAll(AppRepository().getServicesByType(it.id!!))
                        serviceAdapter.notifyDataSetChanged()
                        loading?.hide()
                    }
                }
            rcv_service_types.adapter = serviceTypeAdapter
            val dividerItemDecoration = DividerItemDecoration(
                this@ServiceActivity,
                LinearLayoutManager.HORIZONTAL
            )
            sort.setOnClickListener {
                SortDialog {
                    when (it) {
                        PRICE.sortId -> services.sortBy {
                            it.price
                        }
                        RATE.sortId -> services.sortByDescending { it.price }
                        DISCOUNT.sortId -> {
                            services.sortByDescending { service ->
                                service.price - (service.promotion_price
                                    ?: service.price)
                            }
                        }
                        NEAR.sortId -> services.sortBy { "%.2".format(it.shop?.distance) }
                    }
                    serviceAdapter.notifyDataSetChanged()
                }.show(supportFragmentManager, "sort")
            }

            rcv_services.addItemDecoration(dividerItemDecoration)
            rcv_services.adapter = serviceAdapter
            serviceTypeAdapter.selectServiceType(selectedServiceType.id!!)
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
