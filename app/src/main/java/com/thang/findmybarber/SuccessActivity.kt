package com.thang.findmybarber

import android.content.Intent
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.thang.findmybarber.model.Booking
import kotlinx.android.synthetic.main.activity_success.*
import java.text.SimpleDateFormat
import java.util.*

class SuccessActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success)

        (booking_check.drawable as Animatable).start()
        booking_home_btn.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }
        val booking = intent.getSerializableExtra("booking") as Booking
        if (booking.service != null)
            service_detail_btn.setOnClickListener {
                startActivity(Intent(this, ServiceDetailActivity::class.java).apply {
                    putExtra("service", booking.service)
                })
            }
        if (booking.service?.shop != null)
            shop_btn.setOnClickListener {
                startActivity(Intent(this, SalonActivity::class.java).apply {
                    putExtra("salon", booking.service?.shop)
                })
            }
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val dayFormat = SimpleDateFormat("yyyy-MM-dd")
        val timeFormat = SimpleDateFormat("HH:mm")
        val formattedDate = dateFormat.parse(booking.service_time)
        booking_service_name.text = booking.service?.name
        booking_service_time.text =
            "Lúc ${timeFormat.format(formattedDate)} Ngày ${dayFormat.format(formattedDate)}"
        booking_service_money.text = "Giá ${formatPrice(booking.price)}đ"
        booking_status.text = when (booking.status) {
            Booking.PENDING -> "Đã đặt"
            Booking.CANCEL -> {
                booking_status.setCompoundDrawablesRelative(
                    resources.getDrawable(R.drawable.ic_cancel_red_36dp),
                    null,
                    null,
                    null
                )
                "Đã huỷ"
            }
            Booking.DONE -> {
                "Đã hoàn thành"
            }
            else -> ""
        }
        booking_shop_address.text = booking.service?.shop?.address
        booking_shop_name.text = booking.service?.shop?.name
        booking_shop_calendar.text =
            "Từ ${
                timeFormat.format(
                    Calendar.getInstance().parseTime(booking.service!!.shop!!.open_time)
                )
            } đến " +
                    "${
                        timeFormat.format(
                            Calendar.getInstance().parseTime(booking.service!!.shop!!.open_time)
                        )
                    }"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
