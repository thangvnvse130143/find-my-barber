package com.thang.findmybarber.apiclient

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AppRetrofit {
    companion object {
        private val retrofit: Retrofit by lazy {
            Retrofit.Builder()
                .addConverterFactory(
                    GsonConverterFactory.create(GsonBuilder().create()))
                .baseUrl("https://isalon-server.herokuapp.com")
                .build()
        }
        val apiClient: AppApiClient by lazy {  retrofit.create(AppApiClient::class.java) }
    }
}