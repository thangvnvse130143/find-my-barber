package com.thang.findmybarber

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.tasks.await
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

fun formatDistance(distance: Double): String {
    val format = DecimalFormat("#.##")
    format.roundingMode = RoundingMode.CEILING
    return format.format(distance)
}

fun formatPrice(price: Int?): String {
//    val formatter = DecimalFormat("###.###")
//    return formatter.format(price?.toLong())
    return String.format(Locale.US, "%,d", price).replace(',', '.')
}

suspend fun getCurrentLocation(): Location? {
    val fusedLocationClient = LocationServices.getFusedLocationProviderClient(ApplicationContext.get()!!)
    if (ActivityCompat.checkSelfPermission(
            ApplicationContext.get()!!,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
            ApplicationContext.get()!!,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED
    ) {
        return null
    }
    return fusedLocationClient.lastLocation.await()

}

fun Calendar.parseTime(timeString: String): Date {
    try {
        time = SimpleDateFormat("dd-MM-yyyy hh:mm:ss").parse(timeString)
    } catch (e: Exception) {
        set(Calendar.HOUR_OF_DAY, timeString.toInt())
        set(Calendar.MINUTE, 0)
    }
    return this.time
}