package com.thang.findmybarber.model

data class Review(
    val id: Int,
    val mark: Double,
    val review: String,
    val user_id: Int,
    val service_id: Int,
    @Transient
    var user: User?
)