package com.thang.findmybarber.model

import android.accounts.AuthenticatorDescription
import java.io.Serializable

data class Store(
    val id: Int,
    var name: String,
    val description: String,
    val banner: String,
    var address: String,
    val user_id: String,
//    @Transient
    var distance: Double,
    var rating: Double,
//    var ratingNumber: Int,
//    @Transient
    var shopImg: Int,
    val open_time: String,
    val close_time: String
): Serializable

//{
//    "id": 11,
//    "name": "serivec",
//    "description": "asdasdad sopop ph",
//    "banner": "",
//    "address": "asd",
//    "user_id": 1,
//    "rating": 5,
//    "open_time": "2000-10-10 02:05:00",
//    "close_time": "2000-10-10 02:05:00"
//}