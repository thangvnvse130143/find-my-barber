package com.thang.findmybarber.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.thang.findmybarber.R
import com.thang.findmybarber.adapter.BookingAdapter
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.fragment_history.*
import kotlinx.android.synthetic.main.fragment_history.view.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass.
 */
class BookingFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        MainScope().launch {
            loading?.show()
            val userId = requireContext().getSharedPreferences(
                requireContext().packageName,
                Context.MODE_PRIVATE
            ).getInt("userId", 0)
            val histories = AppRepository().getBookings(userId)
            view.rcv_booking.adapter = BookingAdapter(histories.apply {
                sortBy { it.service_time }
            })
            loading?.hide()
        }
    }

}
