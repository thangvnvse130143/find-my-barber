package com.thang.findmybarber.fragments


import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.smarteist.autoimageslider.IndicatorAnimations
import com.smarteist.autoimageslider.SliderAnimations

import com.thang.findmybarber.R
import com.thang.findmybarber.ServiceActivity
import com.thang.findmybarber.adapter.ImagePagerAdapter
import com.thang.findmybarber.adapter.ImageSliderAdapter
import com.thang.findmybarber.adapter.ServiceAdapter
import com.thang.findmybarber.adapter.ServiceTypeAdapter
import com.thang.findmybarber.model.Service
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.fragment_home.view.loading
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)
//        view.image_view_pager.adapter = ImagePagerAdapter(
//            requireContext(),
//            listOf(R.raw.discount, R.raw.discount2, R.raw.discount3, R.raw.discount4)
//        )
        val serviceTypes = AppRepository().getServiceType()

        val dividerItemDecoration = object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                outRect.left = 36
            }
        }

        view.services_rcv.apply {
            setHasFixedSize(true)
            adapter = ServiceTypeAdapter(serviceTypes, onItemClick = {
                startActivity(
                    Intent(
                        requireContext(),
                        ServiceActivity::class.java
                    ).apply { putExtra("serviceType", it) })
            })
            addItemDecoration(dividerItemDecoration)
        }
//        setViewForShops(view)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setViewForServices(view)
        super.onViewCreated(view, savedInstanceState)
    }

    private fun setViewForServices(view: View) {
        MainScope().launch {
            loading?.show()
            val services = AppRepository().getServices().toMutableList()
            view.suggest_service_rcv.apply {
                setHasFixedSize(true)
                adapter = ServiceAdapter( services.shuffled().toMutableList(), R.layout.service_card_view)
            }

            view.near_service_rcv.apply {
                setHasFixedSize(true)
                val nearServices = ArrayList<Service>().apply { addAll(services) }
                nearServices.sortBy { it.shop?.distance }
                adapter = ServiceAdapter(nearServices, R.layout.service_card_view)
            }

            view.rate_service_rcv.apply {
                setHasFixedSize(true)
                val rateServices = ArrayList<Service>().apply { addAll(services) }
                rateServices.sortBy { it.rating }
                adapter = ServiceAdapter(rateServices, R.layout.service_card_view)
            }

            view.sale_service_rcv.apply {
                setHasFixedSize(true)
                var saleServices: MutableList<Service> = ArrayList<Service>().apply { addAll(services) }
                saleServices = saleServices.filter {
                    it.promotion_price != null && it.promotion_price != 0
                }.sortedByDescending {
                    it.price - if (it.promotion_price == null) it.price else it.promotion_price!!
                }.toMutableList()
                adapter = ServiceAdapter(saleServices, R.layout.service_card_view)
                view.imageSlider.sliderAdapter = ImageSliderAdapter(listOf(R.raw.discount, R.raw.discount2, R.raw.discount3, R.raw.discount4), saleServices)
                view.imageSlider.scrollTimeInSec = 4
                view.imageSlider.setIndicatorAnimation(IndicatorAnimations.WORM)
                view.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
                view.imageSlider.startAutoCycle()
            }

            val viewedIds = requireContext().getSharedPreferences(
                requireContext().packageName,
                Context.MODE_PRIVATE
            ).getStringSet("viewedIds", HashSet<String>())
            if (viewedIds.isNullOrEmpty()) view.viewed.visibility = View.GONE
            else {
                view.viewed_service_rcv.apply {
                    setHasFixedSize(true)
                    adapter = ServiceAdapter(services.filter {
                        viewedIds.contains(it.id.toString())
                    }.toMutableList(), R.layout.service_card_view)
                }
            }
            loading?.hide()
        }
    }

}
