package com.thang.findmybarber

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import com.thang.findmybarber.adapter.RatingAdapter
import com.thang.findmybarber.adapter.ServiceSlotAdapter
import com.thang.findmybarber.model.Booking
import com.thang.findmybarber.model.Review
import com.thang.findmybarber.model.Service
import com.thang.findmybarber.repository.AppRepository
import kotlinx.android.synthetic.main.activity_service_detail.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class ServiceDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_detail)
        val userId =
            getSharedPreferences(packageName, Context.MODE_PRIVATE).getInt("userId", 0)
        val service = intent.getSerializableExtra("service") as Service
        MainScope().launch {
            if (service.shop == null) service.shop = AppRepository().getStore(service.store_id, getCurrentLocation())
            service_name.text = service.name
            service.shop?.let { shop_address.text = it.address }
            service_duration.text = "${service.duration} phút"
            service_description.text = service.detail
            service_price.text = "${formatPrice(service.price)}đ"
            rating.text = service.rating.toString()
            rating_number?.text = service.ratingNumber.toString()

//            if (service.promotion_price != 0) {
//                service_price_crossed.text = "${service.price}đ"
//                service_price.text = "${service.promotion_price}đ"
//            } else {
//                service_price_crossed.visibility = View.GONE
//                service_price.text = "${service.price}đ"
//            }
            if (!service.image.isNullOrEmpty()) {
                val image = Picasso.get().load(service.image)
                image?.into(service_image)
            } else {
                service.dummy_image?.let { service_image.setImageResource(it) }
            }
            service_price_crossed.visibility = View.GONE
            service_price.text = "${formatPrice(service.price)}đ"
            btn_shop.text = "Xem thông tin ${service.shop?.name}"
            btn_shop.setOnClickListener {
                startActivity(Intent(this@ServiceDetailActivity, SalonActivity::class.java).apply {
                    putExtra("salon", service.shop)
                })
            }
        }
        loadReviews(service, userId)
        service_date_picker.apply {
            minDate = Calendar.getInstance().apply { add(Calendar.DAY_OF_MONTH, 1) }.timeInMillis
            maxDate =
                Calendar.getInstance().apply { add(Calendar.DAY_OF_MONTH, 8) }.timeInMillis
        }
        MainScope().launch {
            if (service.shop == null) service.shop = AppRepository().getStore(service.store_id, getCurrentLocation())
            val slots = AppRepository().getSlot(service)
            val adapter = ServiceSlotAdapter(slots) {
                if (service.promotion_price != 0 && it.discount != 0 && it.discount != null) {
                    service_price_crossed.text = "${formatPrice(service.price)}đ"
                    service_price.text = "${formatPrice(service.promotion_price)}đ"
                    service_price_crossed.visibility = View.VISIBLE
                } else {
                    service_price_crossed.visibility = View.GONE
                    service_price.text = "${formatPrice(service.price)}đ"
                }
            }
            service_slots_rcv.adapter = adapter
            service.rating?.let { rating_bar.rating = it.toFloat() }
            rating_btn.setOnClickListener {
                MainScope().launch {
                    AppRepository().createReview(
                        Review(
                            0,
                            rating_rating_bar.rating.toDouble(),
                            review_content.text.toString(),
                            userId,
                            service.id,
                            null
                        )
                    )
                    loadReviews(service, userId)
                    rating_rating_bar.rating = 0f
                    review_content.setText("")
                    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    inputMethodManager.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
                }

            }

            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            service_order_btn.setOnClickListener {
                if (adapter.selectedPos < 0) {
                    AlertDialog.Builder(this@ServiceDetailActivity)
                        .setTitle("Chọn slot")
                        .setMessage("Bạn chưa chọn slot")
                        .setPositiveButton("OK") { dialog, _ -> dialog.dismiss() }
                        .show()
                    return@setOnClickListener
                }
                val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                val selectedSlot = Calendar.getInstance().apply {
                    set(
                        service_date_picker.year,
                        service_date_picker.month,
                        service_date_picker.dayOfMonth,
                        slots[adapter.selectedPos].time.get(Calendar.HOUR_OF_DAY),
                        slots[adapter.selectedPos].time.get(Calendar.MINUTE)
                    )
                }

                val booking = Booking(
                    id = 0,
                    service_id = service.id,
                    status = Booking.PENDING,
                    created_time = dateFormat.format(Calendar.getInstance().time),
                    user_id = userId,
                    service_time = dateFormat.format(selectedSlot.time),
                    price = if (slots[adapter.selectedPos].discount == null) service.price else service.promotion_price!!,
                    service = service
                )
                MainScope().launch {
                    AppRepository().book(booking)
                    startActivity(
                        Intent(
                            this@ServiceDetailActivity,
                            SuccessActivity::class.java
                        ).apply {
                            putExtra("booking", booking)
                        })
                }
            }
        }
    }

    private fun loadReviews(service: Service, userId: Int) {
        MainScope().launch {
            val reviews = AppRepository().getServiceReviews(service.id)
            rating_rcv.adapter = RatingAdapter(reviews.toMutableList(), userId)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
