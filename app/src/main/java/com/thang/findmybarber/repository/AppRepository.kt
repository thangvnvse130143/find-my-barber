package com.thang.findmybarber.repository

import android.location.Location
import android.util.Log
import com.google.maps.DistanceMatrixApi
import com.google.maps.GeoApiContext
import com.google.maps.GeocodingApi
import com.thang.findmybarber.R
import com.thang.findmybarber.apiclient.AppRetrofit
import com.thang.findmybarber.getCurrentLocation
import com.thang.findmybarber.model.*
import com.thang.findmybarber.parseTime
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.invoke
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random


class AppRepository {
    val apiClient = AppRetrofit.apiClient
    val hottoc =
        arrayListOf(R.raw.hot_toc1, R.raw.hot_toc2, R.raw.hot_toc3, R.raw.hot_toc4, R.raw.hot_toc5)
    val goidau = arrayListOf(R.raw.goi_dau, R.raw.goi_dau2, R.raw.goi_dau3, R.raw.goi_dau4)
    val nhuomtoc =
        arrayListOf(R.raw.nhuom_toc, R.raw.nhuom_toc2, R.raw.nhuom_toc3, R.raw.nhuom_toc4)
    val duoitoc = arrayListOf(R.raw.duoi_toc_1, R.raw.duoi_toc_2, R.raw.duoi_toc_3)
    val uontoc = arrayListOf(R.raw.uon_toc_1, R.raw.uon_toc_2, R.raw.uon_toc_3)
    val shops = arrayListOf(
        R.raw.shop,
        R.raw.shop2,
        R.raw.shop3,
        R.raw.shop4,
        R.raw.shop5,
        R.raw.shop6,
        R.raw.shop7,
        R.raw.shop8,
        R.raw.shop9,
        R.raw.shop10
    )

    suspend fun getStore(storeId: Int, location: Location?, stores: List<Store> = ArrayList()): Store? = Dispatchers.IO {
        val store = if (stores.isEmpty()) apiClient.getStore(storeId) else stores.first { it.id == storeId }
        store.apply {
//            shopImg = shops[Random.nextInt(0, shops.size - 1)]
            val context = GeoApiContext.Builder()
                .apiKey("AIzaSyAxzHPTveK1x4CYcTXSW3YWQ_m9u3VtWkE")
                .build()
            try {
                val results = GeocodingApi.geocode(
                    context,
                    address
                ).await()
                val desLocation = results[0].geometry.location
                distance = try {
                    if (location != null) {
                        val distanceResult = DistanceMatrixApi.getDistanceMatrix(
                            context,
                            arrayOf("${location.latitude},${location.longitude}"),
                            arrayOf("${desLocation.lat},${desLocation.lng}")
                        ).await()
                        distanceResult.rows.first().elements.first().distance.inMeters.toDouble() / 1000
                    } else {
                        Random.nextDouble(1.0, 5.0)
                    }
                } catch (ex: Exception) {
                    Random.nextDouble(1.0, 5.0)
                }
            } catch (e: Exception) {
                distance = Random.nextDouble(1.0, 5.0)
            }
        }
    }

    fun getServiceType(): MutableList<ServiceType> {
        return ArrayList<ServiceType>().apply {
            add(ServiceType("Hớt tóc", R.drawable.cattoc, 1))
            add(ServiceType("Duỗi tóc", R.drawable.duoitoc, 2))
            add(ServiceType("Gội đầu", R.drawable.goidau, 3))
            add(ServiceType("Nhuộm tóc", R.drawable.nhuomtoc, 4))
            add(ServiceType("Uốn tóc", R.drawable.uontoc, 5))
        }
    }

    fun getCoupons(): MutableList<Coupon> {
        return ArrayList<Coupon>().apply {
            add(Coupon(img = R.raw.discount))
            add(Coupon(img = R.raw.discount2))
            add(Coupon(img = R.raw.discount3))
            add(Coupon(img = R.raw.discount4))
        }
    }

    suspend fun getServices(storeId: Int? = null) = Dispatchers.IO {
        try {
            val location = getCurrentLocation()
            val stores = apiClient.getStores()
            var services =
                if (storeId == null) apiClient.getServices() else apiClient.getServices(storeId)
            services = services.toMutableList().apply { removeAll { it.store_id == 0 } }
            services.forEach {
                it.shop = getStore(it.store_id, location, stores)
//                it.ratingNumber = 3
                it.dummy_image = when (it.type) {
                    1 -> hottoc[Random.nextInt(0, hottoc.size - 1)]
                    4 -> nhuomtoc[Random.nextInt(0, nhuomtoc.size - 1)]
                    3 -> goidau[Random.nextInt(0, goidau.size - 1)]
                    2 -> duoitoc[Random.nextInt(0, duoitoc.size - 1)]
                    5 -> uontoc[Random.nextInt(0, uontoc.size - 1)]
                    else -> goidau[Random.nextInt(0, goidau.size - 1)]
                }
            }
            services
        } catch (ex: Exception) {
            Log.e("MYAPP", "exception", ex)
            ArrayList<Service>()
        }
    }

    suspend fun getServicesByType(typeId: Int) = Dispatchers.IO {
        try {
            val location = getCurrentLocation()
            val stores = apiClient.getStores()
            val services = apiClient.getServicesByType(typeId)
            services.forEach {
                it.shop = getStore(it.store_id, location, stores)
//            it.ratingNumber = 3
                it.dummy_image = when (it.type) {
                    1 -> hottoc[Random.nextInt(0, hottoc.size - 1)]
                    4 -> nhuomtoc[Random.nextInt(0, nhuomtoc.size - 1)]
                    3 -> goidau[Random.nextInt(0, goidau.size - 1)]
                    2 -> duoitoc[Random.nextInt(0, duoitoc.size - 1)]
                    5 -> uontoc[Random.nextInt(0, uontoc.size - 1)]
                    else -> goidau[Random.nextInt(0, goidau.size - 1)]
                }
            }
            services
        } catch (ex: Exception) {
            Log.e("MYAPP", "exception", ex)
            ArrayList<Service>()
        }
    }


    fun getSlot(service: Service): MutableList<Slot> {

        val slots = ArrayList<Slot>()
        service.promotion_time = service.promotion_time?.trim()
        val promotions = try {
            if (service.promotion_time?.isNotEmpty()!!) service.promotion_time?.split(",")?.map {
                Calendar.getInstance().apply { time = SimpleDateFormat("HH:mm").parse(it) }
            } else null
        } catch (ex: Exception) {
            null
        }

        val initSlot = Calendar.getInstance().apply {
            time = parseTime(service.shop!!.open_time)
        }
        val endSlot = Calendar.getInstance().apply {
            time = parseTime(service.shop!!.close_time)
        }
        var counter = 0
        do {
            val date = initSlot.time
            val dateCalendar = Calendar.getInstance().apply { time = date }
            val currentPromotion = promotions?.firstOrNull { calendar ->
                calendar.get(Calendar.HOUR_OF_DAY).equals(dateCalendar.get(Calendar.HOUR_OF_DAY))
                        && calendar.get(Calendar.MINUTE).equals(dateCalendar.get(Calendar.MINUTE))
            }
            slots.add(
                Slot(
                    dateCalendar,
                    if (counter % 3 == 0) SlotStatus.UNSELECTABLE else SlotStatus.NOT_SELECTED,
                    if (currentPromotion != null) service.discount else null
                )
            )
            counter++
            initSlot.add(Calendar.MINUTE, 30)
        } while (initSlot.before(endSlot))
        return slots

    }

    suspend fun login(user: User) = Dispatchers.IO {
        try {
            apiClient.login(user)
        } catch (ex: Exception) {
            Log.e("MYAPP", "exception", ex)
            null
        }
    }

    suspend fun signUp(user: User) = Dispatchers.IO {
        try {
            apiClient.signUp(user)
        } catch (ex: Exception) {
            Log.e("MYAPP", "exception", ex)
            null
        }
    }

    suspend fun changePassword(user: User) = Dispatchers.IO {
        try {
            apiClient.updateUser(user)
        } catch (ex: Exception) {
            Log.e("MYAPP", "exception", ex)
            null
        }
    }

    suspend fun getUserByUsername(username: String) = Dispatchers.IO {
        try {
            apiClient.getUserByUsername(username)
        } catch (ex: Exception) {
            Log.e("MYAPP", "exception", ex)
            null
        }
    }

    suspend fun getBookings(userId: Int) = Dispatchers.IO {
        try {
            val bookings = apiClient.getBooking(userId).toMutableList()
//        val bookings = ArrayList<Booking>()
//        if (bookings.isEmpty()) {
//            val services = getServices(91)
//            bookings.add(Booking(services[0], 311, Booking.PENDING, "2020-04-08 10:30:19", "2020-04-08 10:30:19", 331, 500000))
//            bookings.add(Booking(services[1], 311, Booking.DONE, "2020-04-08 10:30:19", "2020-04-08 10:30:19", 331, 450000))
//            bookings.add(Booking(services[2], 311, Booking.CANCEL, "2020-04-08 10:30:19", "2020-04-08 10:30:19", 331, 400000))
//        }
            bookings.forEach {
                it.service = apiClient.getService(it.service_id)
                it.service!!.shop = apiClient.getStore(it.service!!.store_id)
            }
            bookings
        } catch (ex: Exception) {
            Log.e("MYAPP", "exception", ex)
            ArrayList<Booking>()
        }
    }

    suspend fun book(booking: Booking) = Dispatchers.IO {
        try {
            apiClient.book(booking)
        } catch (ex: Exception) {
            Log.e("MYAPP", "exception", ex)
            null
        }
    }

    suspend fun updateBook(booking: Booking) = Dispatchers.IO {
        try {
            val response = apiClient.updateBook(booking)
            response.code()
        } catch (ex: Exception) {
            Log.e("MYAPP", "exception", ex)
            null
        }
    }

    suspend fun getServiceReviews(id: Int) = Dispatchers.IO {
        try {
            val reviews = apiClient.getServiceReview(id)
            reviews.forEach {
                it.user = apiClient.getUser(it.user_id)
                it.user?.name
            }
            reviews
        } catch (ex: Exception) {
            Log.e("MYAPP", "exception", ex)
            ArrayList<Review>()
        }
    }

    suspend fun createReview(review: Review) = Dispatchers.IO {
        try {
            apiClient.createServiceReview(review)
        } catch (ex: Exception) {
            Log.e("MYAPP", "exception", ex)
            null
        }
    }

    suspend fun updateReview(review: Review) = Dispatchers.IO {
        try {
            apiClient.updateServiceReview(review)
        } catch (ex: Exception) {
            Log.e("MYAPP", "exception", ex)
            null
        }
    }

    suspend fun deleteReview(id: Int) = Dispatchers.IO {
        try {
            apiClient.deleteServiceReview(id)
        } catch (ex: Exception) {
            Log.e("MYAPP", "exception", ex)
            null
        }
    }

    companion object {
        fun formatDistance(distance: Double): String {
            val format = DecimalFormat("#.##")
            format.roundingMode = RoundingMode.CEILING
            return format.format(distance)
        }
    }
}