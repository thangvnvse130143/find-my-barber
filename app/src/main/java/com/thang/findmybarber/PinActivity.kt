package com.thang.findmybarber

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_pin.*

class PinActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pin)
        otp_view.setOtpCompletionListener {
            if (it!!.contentEquals("123456")) {
                startActivity(Intent(this, PasswordRecoverActivity::class.java))
                finish()
            } else {
                otp_view.error = "Mã PIN không hợp lệ"
            }
        }
        link_forgot_password.setOnClickListener {
            finish()
        }
    }
}
